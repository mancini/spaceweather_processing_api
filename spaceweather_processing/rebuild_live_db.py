
from argparse import ArgumentParser
from pymongo import MongoClient
from configparser import ConfigParser
from pymongo.collection import Collection
import requests
import logging
import html
import os
from glob import glob

config_path = os.environ.get('SW_CONFIG', './solar.config')
logging.basicConfig(format='%(asctime)s %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

def parse_args():
    parser = ArgumentParser('Rebuild live monitor database')
    parser.add_argument('directory', help='Directory where live monitor plots are stored')
    return parser.parse_args()

def list_observations(path):
    dirs = os.listdir(path)
    obs = []
    for dir_name in dirs:
        if not os.path.isdir(os.path.join(path,dir_name)):
            continue

        rt_path = os.path.join(path, dir_name, 'rt')
        if os.path.exists(rt_path):
            dir_types = os.listdir(rt_path)
            if '15min' in dir_types or 'full' in dir_types:
                obs.append(dir_name.lstrip('L'))
    return obs

def path_to_obs(path, sas_id):
    return os.path.join(path, 'L'+sas_id, 'rt')

def time_integrations_in_path(path):
    return ['15min', 'full']


def f_name_to_surl(f_name):
    baseurl = 'https://spaceweather.astron.nl'
    return f_name.replace('/spaceweather', baseurl)

def update_single(obs, tmss, path, database):
    logger.info('Getting metadata for %s', obs)
    metadata = TMSSClient.transform_specification_to_metadata(tmss.get_obs_specification(obs))
    logger.info('Got metadata for %s', obs)
    obs_path = path_to_obs(path, obs)
    for integration in time_integrations_in_path(obs_path):
        fits_files = sorted(glob(obs_path + '/' + integration + '/*.fits'))

        for preview_file in fits_files:
            entry = metadata.copy()
            date, time = preview_file.replace('partial_', '').replace('.fits', '').split('_')[1:3]
            time = time[:2] + ':' + time[2:]
            t_stamp = date + 'T' + time
            entry['_id'] = t_stamp + '_' + obs 
            entry['timestamp'] = t_stamp
            entry['fits_file'] = preview_file
            entry['png_file'] = preview_file.replace('.fits', '.png')
            entry['type'] = integration
            entry['partial'] = preview_file.startswith('partial')
            entry['fits_url'] = f_name_to_surl(entry['fits_file'])
            entry['png_url'] = f_name_to_surl(entry['png_file'])
            database.live_db['previews'].update_one({'_id': entry['_id']},  {"$set":entry}, upsert=True)


def main():
    args = parse_args()
    observations = list_observations(args.directory)
    logger.info('Observations listed')
    database = DB()
    tmss = TMSSClient.load_from_config()
    logger.info('Tmss loaded')
    for obs in observations:
        update_single(obs, tmss, args.directory, database)
            
def path_to_dict(obj, path):
    leaf = obj
    for leaf_name in path.split('.'):
        leaf = leaf[leaf_name]
    return leaf

class TMSSClient:
    def __init__(self, username, password):
        self.api_url = 'https://tmss.lofar.eu/api'
        self.session = requests.session()
        self.username = username
        self.password = password
        self.open()

    @staticmethod
    def load_from_config():
        parser = ConfigParser()
        parser.read(config_path)
        tmss_config = parser['TMSS']
        return TMSSClient(tmss_config['username'], tmss_config['password'])

    def is_sas_id_solar(self, sas_id):
        subtask_meta = self.get_obs_specification(sas_id)
        if subtask_meta:
            return subtask_meta['specifications_doc']['stations']['digital_pointings'] > 1
        else:
            return False
        
    def get_obs_specification(self, sas_id):
        subtask_meta = self.session.get(f'https://tmss.lofar.eu/api/subtask/{sas_id}')
        if subtask_meta.ok:
            return subtask_meta.json()
        else:
            return None
        
    @staticmethod
    def transform_specification_to_metadata(specification):
        fields_to_keep = {
            'id': 'task_id',
            'actual_on_sky_start_time': 'observation_start_time',
            'actual_on_sky_stop_time': 'observation_stop_time',
            'duration': 'duration',
            'on_sky_duration': 'on_sky_duration',
            'scheduled_central_lst': 'scheduled_lst',
            'specifications_doc.stations': 'configuration'
        }
        return {transformed_field:path_to_dict(specification, field_name) for field_name, transformed_field in fields_to_keep.items()}

    def open(self):
        '''open the request session and login'''

        self.session.__enter__()
        self.session.verify = True
        self.session.headers['Accept'] = 'application/json'
        self.session.headers['Accept-Encoding'] = 'gzip'
        # get authentication page of OIDC through TMSS redirect
        response = self.session.get(self.api_url.replace('/api', '/oidc/authenticate/'), allow_redirects=True)
        for resp in response.history:
            logger.info("via %s: %s" % (resp.status_code, resp.url))
        logger.info("got %s: %s" % (response.status_code, response.url))
        # post user credentials to login page, also pass csrf token if present
        if 'csrftoken' in self.session.cookies:
            # Mozilla OIDC provider
            csrftoken = self.session.cookies['csrftoken']
            data = {'username': self.username, 'password': self.password, 'csrfmiddlewaretoken': csrftoken}
            response = self.session.post(url=response.url, data=data, allow_redirects=True)
        else:
            # Keycloak
            content = response.content.decode('utf-8')
            if 'action' not in content:
                raise Exception('Could not determine login form action from server response: %s' % content)
            action = content.split('action="')[1].split('"')[0]
            data = {'username': self.username, 'password': self.password, 'credentialId': ''}
            response = self.session.post(url=html.unescape(action), data=data, allow_redirects=True)

        for resp in response.history:
            logger.info("via %s: %s" % (resp.status_code, resp.url))
        logger.info("got %s: %s" % (response.status_code, response.url))

        # raise when sth went wrong
        if "The username and/or password you specified are not correct" in response.content.decode('utf8') \
                or "Invalid username or password" in response.content.decode('utf8'):
            raise ValueError("The username and/or password you specified are not correct")
        if response.status_code != 200:
            raise ConnectionError(response.content.decode('utf8'))


class DB:
    def __init__(self):
        self.live_db = ''
        self.configure()
        self.live_db: Collection = MongoClient('mongodb://localhost')[self.live_db]

    def append_time_sample(self, type, integration,  fits_file, png_file):
        pass

    def configure(self):
        parser = ConfigParser()
        parser.read(config_path)
        print(config_path)
        print(parser.sections())
        self.live_db = parser['LIVE']['storage_db']

if __name__ == '__main__':
    main()